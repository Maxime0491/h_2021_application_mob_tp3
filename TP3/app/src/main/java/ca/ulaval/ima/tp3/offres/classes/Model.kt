package ca.ulaval.ima.tp3.offres.classes

import com.google.gson.annotations.SerializedName

data class Model(
    @SerializedName("id") val idModele : Int,
    @SerializedName("brand") val brandModele : Brand,
    @SerializedName("name") val namemodele: String
)