package ca.ulaval.ima.tp3

import android.os.Bundle
import android.os.Parcelable
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.offres.classes.Model
import ca.ulaval.ima.tp3.offres.fragments.ModelFragment
import ca.ulaval.ima.tp3.offres.fragments.modellFragment

class MainActivity : AppCompatActivity() , Communicator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

    }

    override fun passDataListModel(brand: Parcelable) {
        val bundle = Bundle()
        bundle.putParcelable("paramlistemodel",  brand)
        val transaction = this.supportFragmentManager.beginTransaction()

        val fragmentModel = ModelFragment()
        fragmentModel.arguments = bundle

        transaction.replace(R.id.framelayout, fragmentModel)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}