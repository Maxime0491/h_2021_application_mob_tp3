package ca.ulaval.ima.tp3.offres.fragments

import android.app.DownloadManager
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.Communicator
import ca.ulaval.ima.tp3.R
import ca.ulaval.ima.tp3.networking.CarAPI
import ca.ulaval.ima.tp3.networking.NetworkCenter
import ca.ulaval.ima.tp3.offres.adapter.ModelRecyclerViewAdapter
import ca.ulaval.ima.tp3.offres.classes.Brand
import ca.ulaval.ima.tp3.offres.classes.BrandParser
import ca.ulaval.ima.tp3.offres.classes.Model
import ca.ulaval.ima.tp3.offres.classes.ModelParser
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


private const val argBrand = "paramlistemodel"

class ModelFragment : Fragment() {

    private lateinit var recycleModel: RecyclerView
    private lateinit var adapter: ModelRecyclerViewAdapter

    //lateinit var brandParser: BrandParser
    var Listmodel = ArrayList<Model>()

    //private lateinit var communicator: Communicator
    val carNetworkCenter = NetworkCenter.buildServce(CarAPI::class.java)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

            var brandParser = it.getParcelable<BrandParser>(argBrand)

            if (brandParser != null) {
                Log.d("testsendmodel", "$brandParser")
                brandParser = brandParser
                Listmodel = getListModel(brandParser.id)
            }
        }

        }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_model, container, false)
            // communicator = activity as Communicator

        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            //code that runs in main
            recycleModel = view.findViewById(R.id.recyclerview_model)
            recycleModel.layoutManager = LinearLayoutManager(activity)

            /*adapter.setOnModelClickListerner {
                val ModelParser = ModelParser(it.idModele, it.namemodele, brandParser.brandName, brandParser)
                communicator.passDataListModel(ModelParser)
            }*/

        }

        fun setAdapter() {
            adapter = ModelRecyclerViewAdapter(Listmodel)
            Log.d("listma2", "$Listmodel")
            recycleModel.adapter = adapter
        }


        fun getListModel(id: Int): ArrayList<Model> {
            carNetworkCenter.listModel(id).enqueue(object :
                Callback<CarAPI.ContentResponse<ArrayList<Model>>> {
                override fun onResponse(
                    call: Call<CarAPI.ContentResponse<ArrayList<Model>>>,
                    response: Response<CarAPI.ContentResponse<ArrayList<Model>>>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.content?.let {
                            for (model in it) {
                                Listmodel.add(model)
                                Log.d("test", "$model")
                            }
                            Log.d("testo", "$Listmodel")

                            activity?.runOnUiThread {
                                setAdapter()
                            }
                        }

                    } else {
                        Log.d("Tp3", "Error")
                    }
                }

                override fun onFailure(
                    call: Call<CarAPI.ContentResponse<ArrayList<Model>>>,
                    t: Throwable
                ) {
                    Log.d("Tp3", "Aucun model pour cette marque $t")
                }
            })

            return Listmodel
        }

}

