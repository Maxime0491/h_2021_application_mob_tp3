package ca.ulaval.ima.tp3.offres.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.R
import ca.ulaval.ima.tp3.offres.classes.Brand
import ca.ulaval.ima.tp3.offres.classes.Model

class ModelRecyclerViewAdapter(private val listModel : List<Model>) :
    RecyclerView.Adapter<ModelRecyclerViewAdapter.ViewHolder>() {
    lateinit var onItemClickListener: ((Model) -> Unit)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textModel: TextView = itemView.findViewById(R.id.text_model)
        var item: Model? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelRecyclerViewAdapter.ViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_model, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ModelRecyclerViewAdapter.ViewHolder, position: Int) {
        val model = listModel[position]
        holder.item = model
        holder.textModel.text = model.namemodele
        holder.itemView.setOnClickListener(){
            onItemClickListener.invoke(model)
        }
    }

    override fun getItemCount(): Int {
        return listModel.size
    }

    fun setOnModelClickListerner(onItemClickListener: (Model) -> Unit){
        this.onItemClickListener = onItemClickListener
    }

}