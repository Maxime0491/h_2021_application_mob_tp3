package ca.ulaval.ima.tp3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import ca.ulaval.ima.tp3.offres.fragments.modellFragment

class ActivityTransfert : AppCompatActivity() , Communicator{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun passDataListModel(brand: Parcelable) {
        val bundle = Bundle()
        bundle.putParcelable("paramlistemodel",  brand)
        val transaction = this.supportFragmentManager.beginTransaction()

        val fragmentModel = modellFragment()
        fragmentModel.arguments = bundle

        transaction.replace(R.id.framelayout, fragmentModel)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}