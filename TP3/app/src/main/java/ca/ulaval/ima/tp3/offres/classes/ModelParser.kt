package ca.ulaval.ima.tp3.offres.classes

import android.os.Parcel
import android.os.Parcelable

class ModelParser (var id: Int, var modelName: String,var brandName: String, var brand: BrandParser?): Parcelable {
    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<ModelParser> {
            override fun createFromParcel(parcel: Parcel) = ModelParser(parcel)
            override fun newArray(size: Int) = arrayOfNulls<ModelParser>(size)
        }
    }
    constructor(parcel: Parcel) : this(
    id = parcel.readInt(),
    modelName = parcel.readString() ?: "",
    brandName = parcel.readString() ?: "",
    brand = parcel.readParcelable<BrandParser>(BrandParser::class.java.classLoader)
    )
    override fun describeContents(): Int {
        return 0
    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(modelName)
        parcel.writeString(brandName)
        parcel.writeParcelable(brand,flags)
    }
}