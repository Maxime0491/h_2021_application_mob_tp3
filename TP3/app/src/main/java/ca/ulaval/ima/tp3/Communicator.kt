package ca.ulaval.ima.tp3

import android.os.Parcelable
import ca.ulaval.ima.tp3.offres.classes.Model

interface Communicator {
    fun passDataListModel(brand : Parcelable)
}