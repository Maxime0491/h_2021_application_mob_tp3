package ca.ulaval.ima.tp3.offres.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ca.ulaval.ima.tp3.offres.classes.Brand
import ca.ulaval.ima.tp3.R

class BrandRecyclerViewAdapter(private val listBrand: List<Brand>) :
    RecyclerView.Adapter<BrandRecyclerViewAdapter.ViewHolder>() {
    lateinit var onItemClickListener: ((Brand) -> Unit)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textOffre: TextView = itemView.findViewById(R.id.text_offre)
        var item: Brand? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_offre, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val brand = listBrand[position]
        holder.item = brand
        holder.textOffre.text = brand.name
        holder.itemView.setOnClickListener(){
            onItemClickListener.invoke(brand)
        }
    }

    override fun getItemCount(): Int {
        return listBrand.size
    }

    fun setOnBrandClickListerner(onItemClickListener: (Brand) -> Unit){
        this.onItemClickListener = onItemClickListener
    }

}